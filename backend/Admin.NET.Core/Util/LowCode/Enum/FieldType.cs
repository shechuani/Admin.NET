﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Furion.Extras.Admin.NET.Util.LowCode.Enum
{
    public enum FieldType
    {
        String,
        Int,
        Decimal,
        Date
    }
}
